import re
import pandas as pd
import hgvs.parser
from hgvs.exceptions import HGVSParseError, HGVSInvalidVariantError
import hgvs.dataproviders.uta
import hgvs.assemblymapper
import hgvs.edit


class EGFRVariant:

    def __init__(self, c_string, p_string):
        self.p_string = p_string
        self.c_string = c_string

        if self.c_string and not self.c_string.startswith("c."):
            self.c_string = "c." + self.c_string
        if not self.p_string.startswith("p."):
            self.p_string = "p." + self.p_string

        self.var_p = None
        self.var_c = None
        self.var_canonical_p = None
        self.errors = []

    def __str__(self):
        return self.format_p()

    def __bool__(self):
        return self.var_p is not None

    def format_p(self,
                 type="original"):
        if self.var_p is None:
            return ""
        if type == "original":
            p_str = self.var_p.format()  # conf={"p_3_letter": False})
        elif type == "canonical":
            if not self.var_canonical_p:
                return "<failed validation>"
            p_str = self.var_canonical_p.format()  # conf={"p_3_letter": False})
        else:
            raise ValueError("Invalid type ", type)
        return p_str.split(":")[1]  # return part after the ":", omit transcript id


class EGFRParser:

    def __init__(self):
        hgvs.config.global_config.formatting.p_3_letter = False
        self.transcript = "NM_005228.3"
        self.parser = hgvs.parser.Parser()
        hdp = hgvs.dataproviders.uta.connect()
        self.mapper = hgvs.assemblymapper.AssemblyMapper(hdp,
                                                         assembly_name='GRCh37',
                                                         alt_aln_method='splign',
                                                         replace_reference=True)

    def create_variant(self,
                       c_string,
                       p_string):
        return self.initialize_variant(EGFRVariant(c_string, p_string))

    def initialize_variant(self,
                           var: EGFRVariant):
        try:
            var.var_c = self.parser.parse_hgvs_variant(self.transcript + ":" + var.c_string)
        except HGVSParseError as e:
            if var.c_string:  # skip for empty strings
                var.errors.append("Parsing Error cDNA: " + str(e))

        try:
            var.var_p = self.parser.parse_hgvs_variant(self.transcript + ":" + var.p_string)
        except HGVSParseError as e:
            if var.p_string:
                var.errors.append("Parsing Error Protein: " + str(e))

        try:
            if var.var_c is not None:
                var.var_canonical_p = self.mapper.c_to_p(var.var_c)
                var.var_canonical_p.posedit.uncertain = False
        except HGVSInvalidVariantError as e:
            var.errors.append("Mapping Error: " + str(e))

        return var


class EGFRClassificationResult:

    def __init__(self):
        self.vars = []
        self.classifications = []
        self.extra_data = pd.Series()

    def __bool__(self):
        return bool(self.vars)

    def __str__(self):
        if not self.vars:
            return ""
        if self.is_compound():
            return "Compound mutation {} -> ({})".format(self.format_variants(), self.format_classifications())
        return "{} -> {}".format(self.vars[0], self.classifications[0])

    def __iter__(self):
        return iter(zip(self.vars, self.classifications))

    def add(self,
            var: EGFRVariant):
        self.vars.append(var)
        self.classifications.append(self.classify(var))

    def is_compound(self):
        return len(self.vars) > 1

    def is_classified(self):
        return self.vars and None not in self.classifications

    def format_variants(self):
        return ", ".join(str(var) for var in self.vars)

    def format_classifications(self):
        return ", ".join(self.format_classification(cl) for cl in self.classifications)

    @staticmethod
    def format_classification(classification):
        if classification:
            return classification
        else:
            return "unclassified"

    def to_dict(self):
        result = {}
        for var, classified in zip(self.vars, self.classifications):
            result[var.p_string] = classified
        return result

    def to_series(self):
        series = pd.Series()
        for i in range(len(self.vars)):
            series["cDNA_{}".format(i)] = self.vars[i].c_string
            series["protein_{}".format(i)] = self.vars[i].p_string
            series["classification_{}".format(i)] = self.format_classification(self.classifications[i])
        return series.append(self.extra_data)

    @staticmethod
    def classify(var: EGFRVariant):
        # -          Exon 19 deletion (short in-frame deletion starting between codon 744 and 752)
        # -          p.L858R
        # -          Exon 19 insertion (insertion of 6 amino acids starting in codon 744 or 745)
        # -          Exon 20 insertion (Insertion between codon 767 and 774)
        # -          “Yang Group 1”: p.G719X, p.L861Q, p.S768I
        # -          p.A763_Y764insFQEA
        # -          other C-helical exon 20 insertions (codon 761 - 766)
        # -          p.T790M alone or in combination
        # -          p.C797S alone or in combination
        # -          compound mutations involving any of the above
        if var.var_p is None:
            return None
        posedit = var.var_p.posedit
        start = posedit.pos.start
        end = posedit.pos.end
        edit = posedit.edit
        edittype = edit.type

        if edittype == "sub":
            if start.pos == 858 and edit.alt == "R":
                return "L858R"
            if start.pos == 719:
                return "G719X"
            if start.pos == 861 and edit.alt == "Q":
                return "L861Q"
            if start.pos == 768 and edit.alt == "I":
                return "S768I"
            if start.pos == 790 and edit.alt == "M":
                return "T790M"
            if start.pos == 797 and edit.alt == "S":
                return "C797S"
        if 744 <= start.pos <= 752 and edittype in ["del", "delins"]:
            return "exon19deletion"
        if 744 <= start.pos <= 746 and edittype == "ins" and posedit.length_change() == 6:
            return "exon19insertion"
        if edittype == "dup" and start.pos == 740 and end.pos == 745:
            return "exon19insertion"
        if 761 <= start.pos <= 766 and edittype in ["ins", "delins", "dup"]:
            if start.pos == 763 and end.pos == 764 and edittype == "ins" and edit.alt == "FQEA":
                return "exon20insertion_A763_Y764insFQEA"
            else:
                return "exon20insertion_c_helix"
        if 767 <= start.pos <= 774 and edittype in ["ins", "delins", "dup"]:
            # "classical" Exon 20 insertion in the loop following the C helix
            return "exon20insertion"

        return None


def load_cologne_data():
    mutations_recurrent_3 = pd.read_excel("../MC_Jule_EGFR_LCGC_ab_2015-3_ExtraCol_OVC_AH_SMB_mut_only_1col.xlsx")
    mutations_recurrent_3.columns=["protein", "cDNA"]
    mutations_recurrent_3["source"] = "Cologne >= 3"
    mutations_recurrent_2 = pd.read_excel("../MC_Jule_EGFR_LCGC_ab_2015-3_ExtraCol_OVC_AH_SMB_mut_only_1col_2mal.xlsx",
                                          header=None)
    mutations_recurrent_2.columns=["protein", "cDNA"]
    mutations_recurrent_2["source"] = "Cologne 2"
    return pd.concat([mutations_recurrent_2, mutations_recurrent_3], axis=0).reset_index(drop=True)


def load_cosmic_data():
    cosmic_egfr = pd.read_csv("../Gene_mutationsMon Jul  8 15_19_47 2019 Lung.csv").rename(
        columns={"CDS Mutation": "cDNA", "AA Mutation": "protein", "Count": "count"}) \
        .loc[:, ["cDNA", "protein", "count"]]
    # remove intronic variants
    cosmic_egfr = cosmic_egfr[cosmic_egfr["protein"] != "p.?"]
    cosmic_egfr["source"] = "Cosmic"

    def best_cdna(cdnas):
        for cdna in cdnas:
            if cdna == "c.?" or not cdna:
                continue
            return cdna
        return ""

    cosmic_egfr = cosmic_egfr.groupby(["protein"]).agg({
        "count": "sum",
        "cDNA": best_cdna
    }).reset_index()

    re_delins = re.compile("(p\.[A-Z\d]+_[A-Z\d]+)(?:>|ins)([A-Z]+)")
    re_del = re.compile("(p\.[A-Z\d]+_[A-Z\d]+)del([A-Z]+)")

    def old_style_notation(protein):
        match = re_delins.fullmatch(protein)
        if match:
            return match.group(1) + "delins" + match.group(2)
        match = re_del.fullmatch(protein)
        if match:
            return match.group(1) + "del"
        return protein
    cosmic_egfr["protein"] = cosmic_egfr["protein"].apply(old_style_notation)

    counts = cosmic_egfr["count"]
    one_percent = counts.sum() / 100
    ranks = counts.rank(ascending=False)
    mutations_recurrent = cosmic_egfr[(
            #(counts >= one_percent) |
            (ranks <= 100)
    )]

    return mutations_recurrent


def classify_data(df):
    parser = EGFRParser()
    extra_columns = [col for col in df.columns if col not in ["protein", "cDNA"]]

    def classify_rows():
        for row in df.index:
            p_line = df.loc[row, "protein"]
            c_line = df.loc[row, "cDNA"]
            if p_line.startswith("p.["):
                p_line = p_line.replace("p.[", "p.").replace(";", ";p.").replace("]", "")

            if c_line.startswith("c.["):
                c_line = c_line.replace("c.[", "c.").replace(";", ";c.").replace("]", "")

            result = EGFRClassificationResult()
            result.extra_data = df.loc[row, extra_columns]
            for p_string, c_string in zip(p_line.split(";"), c_line.split(";")):
                var = parser.create_variant(c_string, p_string)
                if var:
                    result.add(var)
                for error in var.errors:
                    print("Parse Error: {}   {}   {}".format(var.c_string, var.p_string, error))
                if not var.errors and var.c_string and var.p_string != var.format_p("canonical"):
                    print("Validation problem: {}   {}   {}".format(var.c_string, var.p_string, var.format_p("canonical")))

            yield result

    return list(classify_rows())


def classify_data_and_save(prefix, recurrent):
    classified = []
    unclassified = []
    invalid = []
    compounds = []
    seen_unclassified_mutations = set()
    for result in classify_data(recurrent):
        if not result:
            invalid.append(result)
        elif result.is_classified():
            classified.append(result)
        else:
            unclassified.append(result)
        if result.is_compound():
            compounds.append(result)

        if result:
            for var, classification in result:
                if not classification:
                    seen_unclassified_mutations.add(str(var))

    print(""
          "{} classified, {} unclassified, {} invalid, {} compounds".format(
        len(classified), len(unclassified), len(invalid), len(compounds)))
    print(""
          "Classified:")
    print("\n".join(str(result) for result in classified))
    print(""
          "Unclassified (at least one):")
    print("\n".join(str(result) for result in unclassified))

    df_classified = pd.concat([result.to_series() for result in classified], axis=1, sort=False).T
    df_unclassified = pd.concat([result.to_series() for result in unclassified], axis=1, sort=False).T

    df_classified.to_csv("../{}-classified.csv".format(prefix), sep=";", index=False, header=True)
    df_unclassified.to_csv("../{}-unclassified.csv".format(prefix), sep=";", index=False, header=True)

    return seen_unclassified_mutations


if __name__ == "__main__":
    # parser = EGFRParser()
    # var = parser.create_variant("c.2217_2234dup", "p.I740_K745dup")
    # print(EGFRClassificationResult.classify(var))
    # exit()
    cosmic_unclassified = classify_data_and_save("cosmic", load_cosmic_data())
    cologne_unclassified = classify_data_and_save("cologne", load_cologne_data())

    pd.Series(sorted(cosmic_unclassified - cologne_unclassified), name="protein").\
        to_csv("../Cosmic unclassified mutations not seen in Cologne.csv", sep=";", index=False, header=True)
    print("Cosmic unclassified mutations not seen in Cologne:")
    print("\n".join(cosmic_unclassified - cologne_unclassified))
    print("Cologne unclassified mutations not seen in Cosmic:")
    print("\n ".join(cologne_unclassified - cosmic_unclassified))

